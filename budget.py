import os
from datetime import datetime
import re
from tabulate import tabulate
import calendar
import asciichartpy

filename = os.path.expanduser("~/budget.txt")


# Colors
class Color:
    RED = "\033[91m"
    GREEN = "\033[92m"
    YELLOW = "\033[93m"
    CYAN = "\033[96m"
    END = "\033[0m"


# Function to add expense
def add_expense(file_path):
    print(
        f"\n{Color.CYAN}Enter expense details (or type 'abort' to cancel):{Color.END}"
    )
    try:
        # Create the budget.txt file if it doesn't exist
        if not os.path.exists(file_path):
            with open(file_path, "w"):
                pass
        with open(file_path, "a") as file:
            while True:
                value = input(f"{Color.YELLOW}Value:{Color.END} ")
                if value.lower() == "abort":
                    print(f"{Color.RED}Expense addition aborted.{Color.END}")
                    return  # Return to main menu
                if re.match(
                    r"^\s*([1-9][0-9]*(\.[0-9]*)?|0\.[0-9]*[1-9][0-9]*)\s*$", value
                ):
                    value = float(value)
                    break
                else:
                    print(
                        f"{Color.RED}Invalid input. Please enter a valid non-zero value.{Color.END}"
                    )
            while True:
                category = input(f"{Color.YELLOW}Category:{Color.END} ")
                if category.lower() == "abort":
                    print(f"{Color.RED}Expense addition aborted.{Color.END}")
                    return  # Return to main menu
                elif not category:
                    print(f"{Color.RED}Category is mandatory.{Color.END}")
                else:
                    break
            comment = input(f"{Color.YELLOW}Comment (optional):{Color.END} ")
            if comment.lower() == "abort":
                print(f"{Color.RED}Expense addition aborted.{Color.END}")
                return  # Return to main menu
            file.write(
                f"{datetime.now().strftime('%b %Y')} - {value:.2f} € - {category} - {comment}\n"
            )
            print(f"{Color.GREEN}\nExpense added successfully.{Color.END}")
    except IOError as e:
        print(f"{Color.RED}Error: {e}{Color.END}")


# Function to display expenses
def display_expenses(file_path):
    try:
        with open(file_path, "r") as file:
            expenses_data = []
            for line in file:
                parts = line.strip().split(" - ")
                if len(parts) == 4:
                    date, value, category, comment = parts
                    expenses_data.append([date, value, category, comment])
                elif len(parts) == 3:  # If there's no comment
                    date, value, category = parts
                    expenses_data.append(
                        [date, value, category, ""]
                    )  # Add empty string for comment
                else:
                    print(f"{Color.RED}Invalid data format: {line.strip()}{Color.END}")
        headers = ["Date", "Value (€)", "Category", "Comment"]
        print(tabulate(expenses_data, headers=headers, tablefmt="grid"))
    except FileNotFoundError:
        print(f"\n{Color.RED}Error: File not found: {file_path}{Color.END}")


def display_total(file_path):
    try:
        with open(file_path, "r") as file:
            total = 0
            for line in file:
                parts = line.split()
                if len(parts) >= 4:
                    try:
                        value = float(parts[3])  # Extract value from the line
                        total += value
                    except ValueError:
                        pass  # Ignore lines where value cannot be converted to float
        print(f"{Color.CYAN}\nTotal expenses: {total:.2f} €{Color.END}")
    except FileNotFoundError:
        print("Error: File not found.")


# Function to delete last insertion
def delete_last_insertion(file_path):
    try:
        with open(file_path, "r+") as file:
            lines = file.readlines()
            if lines:
                last_line = lines.pop()
                print(
                    f"{Color.YELLOW}Deleted last insertion: {last_line.strip()}{Color.END}"
                )
                file.truncate(0)
                file.seek(0)
                file.writelines(lines)
            else:
                print(
                    f"{Color.RED}There is no data to remove. The file is empty.{Color.END}"
                )
    except FileNotFoundError:
        print(f"\n{Color.RED}The file does not exist: {file_path}{Color.END}")
    except (PermissionError, IOError) as e:
        print(f"{Color.RED}Error: {e}{Color.END}")


# Function to clear the screen
def clear_screen():
    os.system("cls" if os.name == "nt" else "clear")


# Function to generate monthly expense graph in terminal
def generate_monthly_expense_graph_terminal(expenses_data):
    if not expenses_data:
        print("No expenses data available.")
        return

    monthly_expenses = {}
    # Aggregate expenses by month
    for expense in expenses_data:
        date = datetime.strptime(expense[0], "%b %Y")
        month = date.month
        year = date.year
        key = (year, month)
        value = float(expense[1].replace(" €", ""))

        if key in monthly_expenses:
            monthly_expenses[key] += value
        else:
            monthly_expenses[key] = value

    # Prepare data for plotting
    x_labels = []
    y_values = []
    total_expense = 0
    num_months = 0
    for year in range(
        min(monthly_expenses.keys())[0], max(monthly_expenses.keys())[0] + 1
    ):
        for month in range(1, 13):
            if (year, month) in monthly_expenses:
                x_labels.append(
                    calendar.month_abbr[month]
                )  # Append only the month abbreviation
                y_values.append(monthly_expenses[(year, month)])
                total_expense += monthly_expenses[(year, month)]
                num_months += 1
            else:
                x_labels.append(
                    calendar.month_abbr[month]
                )  # Append only the month abbreviation
                y_values.append(0)

    # Get the current year and month
    current_year = datetime.now().year
    current_month = datetime.now().month

    # Convert calendar.month_abbr to a list for slicing
    month_abbr_list = list(calendar.month_abbr)

    # Get the index of the current month in the list of month abbreviations
    current_month_index = month_abbr_list.index(calendar.month_abbr[current_month])

    # Create a new list of month abbreviations starting from the current month
    months_from_current = (
        month_abbr_list[current_month_index:] + month_abbr_list[1:current_month_index]
    )

    # Plot the graph
    chart = asciichartpy.plot(
        y_values,
        {"height": 20, "offset": 3, "padding": 5, "colors": [asciichartpy.green]},
    )
    print("\nMonthly Expenses:\n")
    print(chart)
    print(f"\n{current_year}/Month:", ", ".join(months_from_current))

    # Print floating-point statistics
    max_expense = max(y_values)
    min_expense = min(y_values)
    avg_expense = total_expense / num_months if num_months > 0 else 0
    print(f"\nMax Expense: {max_expense:.2f} €")
    print(f"Min Expense: {min_expense:.2f} €")
    print(f"Avg Expense: {avg_expense:.2f} €")


# Main function
def main():
    while True:
        clear_screen()  # Clear the screen
        print("\nMenu:")
        print(f"{Color.YELLOW}a){Color.END} Add expense")
        print(f"{Color.YELLOW}d){Color.END} Display expenses")
        print(f"{Color.YELLOW}c){Color.END} Display the total")
        print(f"{Color.YELLOW}g){Color.END} Generate monthly expense graph (terminal)")
        print(f"{Color.YELLOW}r){Color.END} Delete last insertion")
        print(f"{Color.YELLOW}q){Color.END} Quit")

        choice = input(f"{Color.CYAN}\nEnter your choice: {Color.END}")

        if choice == "a":
            add_expense(filename)
            input(f"\n{Color.CYAN}Press Enter to continue...{Color.END}")
        elif choice == "d":
            display_expenses(filename)
            input(f"\n{Color.CYAN}Press Enter to continue...{Color.END}")
        elif choice == "c":
            display_total(filename)
            input(f"\n{Color.CYAN}Press Enter to continue...{Color.END}")
        elif choice == "g":
            if os.path.exists(filename):
                with open(filename, "r") as file:
                    expenses_data = [line.strip().split(" - ")[:2] for line in file]
                generate_monthly_expense_graph_terminal(expenses_data)
            else:
                print("No expenses data available.")
            input(f"\n{Color.CYAN}Press Enter to continue...{Color.END}")
        elif choice == "r":
            delete_last_insertion(filename)
            input(f"\n{Color.CYAN}Press Enter to continue...{Color.END}")
        elif choice == "q":
            print(f"\n{Color.YELLOW}Exiting program.{Color.END}")
            break
        else:
            print(f"\n{Color.RED}Invalid choice. Please choose again.{Color.END}")


if __name__ == "__main__":
    main()
