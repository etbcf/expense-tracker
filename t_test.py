import os
from unittest.mock import patch
import re
import tempfile
import pytest

# Import functions from the script
from budget import add_expense, display_expenses, display_total, delete_last_insertion


@pytest.fixture
def temp_filename():
    # Create a temporary file for testing
    temp_file = tempfile.NamedTemporaryFile(mode='w+', delete=False)
    yield temp_file.name
    # Remove the temporary file after testing
    temp_file.close()
    os.unlink(temp_file.name)


def test_add_expense(temp_filename):
    # Mock user input
    with patch('builtins.input', side_effect=['10', 'Food', 'Lunch']):
        # Call the function
        add_expense(temp_filename)

        # Check if the expense was added correctly
        with open(temp_filename, 'r') as file:
            lines = file.readlines()
            assert len(lines) == 1

            # Print the content of the line
            print("Line content:", lines[0])

            # Print the regular expression pattern
            pattern = r'\w{3} \d{4} - 10\.00( €)? - Food - Lunch\n'
            print("Regex pattern:", pattern)

            # Match the line with the pattern
            match = re.match(pattern, lines[0])
            print("Match result:", match)

            # Assert the match
            assert match is not None, f"No match found for pattern: {pattern}"


def test_display_expenses(temp_filename, capsys):
    # Write sample expenses to the temporary file
    with open(temp_filename, 'w') as file:
        file.write("Jan 2023 - 10.00 € - Food - Lunch\n")
        file.write("Jan 2023 - 20.00 € - Transportation - Taxi\n")

    # Call the function with the temporary filename
    display_expenses(temp_filename)

    # Capture stdout
    captured = capsys.readouterr()

    # Remove ANSI escape codes from captured output for comparison
    captured_output_without_color = remove_ansi_escape_codes(captured.out)

    # Define the expected output
    expected_output = (
        "+----------+-------------+----------------+-----------+\n"
        "| Date     | Value (€)   | Category       | Comment   |\n"
        "+==========+=============+================+===========+\n"
        "| Jan 2023 | 10.00 €     | Food           | Lunch     |\n"
        "+----------+-------------+----------------+-----------+\n"
        "| Jan 2023 | 20.00 €     | Transportation | Taxi      |\n"
        "+----------+-------------+----------------+-----------+\n"
    )

    assert captured_output_without_color.strip() == expected_output.strip()


def remove_ansi_escape_codes(text):
    """
    Removes ANSI escape codes from text.
    """
    ansi_escape = re.compile(r'\x1B(?:[@-Z\\-_]|\[[0-?]*[ -/]*[@-~])')
    return ansi_escape.sub('', text)


def test_display_total(temp_filename, capsys):
    # Write sample expenses to the temporary file
    with open(temp_filename, 'w') as file:
        file.write("Jan 2023 - 10.00 € - Food - Lunch\n")
        file.write("Jan 2023 - 30.00 € - Transportation - Taxi\n")

    # Call the function with the temporary filename
    display_total(temp_filename)

    # Capture stdout
    captured = capsys.readouterr()

    # Remove ANSI escape codes from expected and captured output
    expected_output = "\nTotal expenses: 40.00 €\n"
    captured_output = captured.out
    expected_output_no_color = remove_ansi_escape_codes(expected_output)
    captured_output_no_color = remove_ansi_escape_codes(captured_output)

    # Compare the outputs without ANSI escape codes
    assert captured_output_no_color == expected_output_no_color


def test_delete_last_insertion(temp_filename):
    # Write sample expenses to the temporary file
    with open(temp_filename, 'w') as file:
        file.write("Jan 2023 - 10.00 € - Food - Lunch\n")
        file.write("Jan 2023 - 20.00 € - Transportation - Taxi\n")

    # Call the function
    delete_last_insertion(temp_filename)

    # Check if the last insertion was deleted
    with open(temp_filename, 'r') as file:
        lines = file.readlines()
        assert len(lines) == 1
        assert lines[0] == "Jan 2023 - 10.00 € - Food - Lunch\n"

    # Call the function again to delete the remaining insertion
    delete_last_insertion(temp_filename)

    # Check if the last insertion was deleted
    with open(temp_filename, 'r') as file:
        lines = file.readlines()
        assert len(lines) == 0
